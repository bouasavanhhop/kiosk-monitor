<?php
date_default_timezone_set("Asia/Bangkok");

class dbconnector {

    public $db;

    //function __construct($host = "10.0.2.121", $dbname = "kiosk", $username = "monitor", $password = "Mon4torSMV2") {
    function __construct($host = "10.0.130.17", $dbname = "kiosk", $username = "monitor", $password = "Mon4torSMV2") {
        $this->db = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    public function Queries($qry, $param = null, $key = null) {
        $stm = $this->db->prepare($qry);
        $res = $stm->execute($param);

        if ($res == false){
            $errdata = json_encode($this->db->errorInfo());
            trigger_error("Oracle Error: $errdata\r\n<br/>At Query: $qry", E_USER_WARNING);
            return false;
        } else {
            $arrayres = array();
            while ($row = $stm->fetch(PDO::FETCH_ASSOC)){
                if ($key != null)
                    $arrayres[$row[$key]] = $row;
                else
                    $arrayres[] = $row;
            }

        }
        return $arrayres;
    }

    public function Query($qry, $param = null) {
        $stm = $this->db->prepare($qry);
        $res = $stm->execute($param);

        if ($res == false){
            $errdata = json_encode($this->db->errorInfo());
            trigger_error("Oracle Error: $errdata\r\n<br/>At Query: $qry", E_USER_WARNING);
            return false;
        } else{
            $rowres = $stm->fetch(PDO::FETCH_ASSOC);
            return $rowres;
        }
    }

    public function execute($qry, $param = null) {
        $stm = $this->db->prepare($qry);
        $res = $stm->execute($param);
        return $res;
    }

    public function beginTransaction(){
        $this->db->beginTransaction();
    }

    public function commit(){
        $this->db->commit();
    }

    public function rollback(){
        $this->db->rollBack();
    }
}
