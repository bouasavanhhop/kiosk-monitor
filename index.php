<?php
/**
 * Created by IntelliJ IDEA.
 * User: Narin
 * Date: 2018-12-18
 * Time: 3:24 PM
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>SmartVAT Kiosk Status</title>
    <link type="text/css" rel="stylesheet" href="css/index.css"/>
    <script src="vendors/vue.min.js"></script>

</head>
<body>
<div id="app" :class="{visible: visible}">

    <div v-if="isLoading" class="loading overlay">
        <div class="overlay__text">Loading . . .</div>
    </div>

    <div v-if="errorMessage" class="error-message">
        <p>{{errorMessage}}</p>
    </div>

    <div class="kiosklist">
        <div v-for="kiosk in formattedKiosks" class="kiosk">
            <h1>{{kiosk.name}} <span class="light">({{kiosk.ip}})</span></h1>

            <!--        todo: waiting for log from database -->

            <!--        <h2>Hardware Status</h2>-->
            <!--        <div class="statuses">-->
            <!--          <div class="status receipt working">Receipt Printer</div>-->
            <!--          <div class="status sticker error">Sticker Printer</div>-->
            <!--          <div class="status webcam">Webcam</div>-->
            <!--          <div class="status cardreader">Card Reader</div>-->
            <!--          <div class="status pinpad">PIN Pad</div>-->
            <!--          <div class="status thb">THB Acceptor</div>-->
            <!--          <div class="status lak">LAK Acceptor</div>-->
            <!--        </div>-->

            <h2>Consumables</h2>
            <div class="consumables">
                <div class="consume receipt"
                     :class="[
              threshold.receipts.danger > kiosk.receipts && kiosk.receipts >= threshold.receipts.warning ? 'warning': '',
              kiosk.receipts >= threshold.receipts.danger ? 'danger' : '']">
                    <p class="refilltime" title="Refill time">{{ kiosk.receiptrefilltime }}</p>
                    <div class="border">
                        <p class="consumetitle">Receipts</p>
                        <p class="consumedata">{{kiosk.receipts}}/{{capacity.receipts}}</p>
                    </div>
                </div>
                <div class="consume lak"
                     :class="[
              threshold.laknotes.danger > kiosk.laknotes && kiosk.laknotes >= threshold.laknotes.warning ? 'warning': '',
              kiosk.laknotes >= threshold.laknotes.danger ? 'danger' : '']">
                    <p class="refilltime" title="Clearing time">{{ kiosk.lakrefilltime }}</p>
                    <div class="border">
                        <p class="consumetitle">LAK Notes</p>
                        <p class="consumedata">{{kiosk.laknotes}}/{{capacity.laknotes}}</p>
                    </div>
                </div>
                <div class="consume thb"
                     :class="[
              threshold.thbnotes.danger > kiosk.thbnotes && kiosk.thbnotes >= threshold.thbnotes.warning ? 'warning': '',
              kiosk.thbnotes >= threshold.thbnotes.danger ? 'danger' : '']">
                    <p class="refilltime" title="Clearing time">{{ kiosk.thbrefilltime }}</p>
                    <div class="border">
                        <p class="consumetitle">THB Notes</p>
                        <p class="consumedata">{{kiosk.thbnotes}}/{{capacity.thbnotes}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="vendors/axios.min.js"></script>

<script>
    Vue.config.devtools = true
    var app = new Vue({
        el: '#app',
        data: {
            visible: true,
            kiosks: [],
            consumableStatus: [],
            capacity: {
                receipts: Number,
                laknotes: Number,
                thbnotes: Number
            },
            threshold: {
                receipts: {
                    warning: Number,
                    danger: Number
                },
                laknotes: {
                    warning: Number,
                    danger: Number
                },
                thbnotes: {
                    warning: Number,
                    danger: Number
                }
            },
            isLoading: true,
            errorMessage: ''
        },
        mounted() {
            this.loadKiosks()
            this.loadCapacity()
            this.loadConsumableStatus()

            window.setInterval(this.loadConsumableStatus, 5 * 60 * 1000)
        },
        computed: {
            defaultCosumableStatus() {
                return {
                    receipts: 0,
                    laknotes: 0,
                    thbnotes: 0
                }
            },
            formattedKiosks() {
                const kiosks = []
                this.kiosks.forEach(kiosk => {
                    // set default kiosk
                    let _kiosk = {
                        ...kiosk,
                        ...this.defaultCosumableStatus,
                    };
                    this.consumableStatus.forEach(_consumableStatus => {
                        if (kiosk.id === _consumableStatus.id) {
                            _kiosk = {
                                ...kiosk,
                                receipts: _consumableStatus['printedreceipts'],
                                laknotes: _consumableStatus['insertedlaks'],
                                thbnotes: _consumableStatus['insertedthbs'],

                                receiptrefilltime: _consumableStatus['receiptrefilltime'],
                                lakrefilltime: _consumableStatus['lakrefilltime'],
                                thbrefilltime: _consumableStatus['thbrefilltime'],
                            }
                        }
                    })
                    kiosks.push(_kiosk)
                })
                return kiosks
            }
        },
        methods: {
            loadKiosks() {
                axios.get('loadkiosks.php')
                    .then(response => this.kiosks = response.data)
                    .catch(err => this.errorMessage = this.createErrorMessage('Kiosks', err))
            },
            loadCapacity() {
                axios.get('loadcapacity.php')
                    .then(response => {
                        this.capacity = this.formatCapacity(response.data.capacity)
                        this.threshold = this.formatThreshold(response.data.capacity)
                    })
                    .catch(err => this.errorMessage = this.createErrorMessage('Capacity and Threshold', err))
            },
            loadConsumableStatus() {
                axios.get('loadcosumablestatus.php')
                    .then(response => this.consumableStatus = response.data)
                    .catch(err => this.errorMessage = this.createErrorMessage('Consumable Status', err))
                    .finally(() => this.isLoading = false)
            },
            createErrorMessage(resource, err) {
                var message = 'Failed to load: ' + resource + '. Please try again later.'
                if (err) {
                    message += ' '
                    message += 'Error: ' + err
                }
                return message
            },
            isLongerThanSixDays(dateInSeconds) {
                return dateInSeconds > 60 * 60 * 24 * 6;
            },
            formatCapacity(capacity) {
                return {
                    receipts: capacity.RECEIPT,
                    laknotes: capacity.LAK,
                    thbnotes: capacity.THB,
                }
            },
            formatThreshold(capacity) {
                return {
                    receipts: {
                        warning: capacity.RECEIPT * 60 / 100,
                        danger: capacity.RECEIPT * 80 / 100,
                    },
                    laknotes: {
                        warning: capacity.LAK * 60 / 100,
                        danger: capacity.LAK * 80 / 100,
                    },
                    thbnotes: {
                        warning: capacity.THB * 60 / 100,
                        danger: capacity.THB * 80 / 100,
                    }
                }
            },
        }
    })

</script>
</body>
</html>
