<?php
header("Access-Control-Allow-Origin: *");

include_once 'dbconnector.php';
$conn = new dbconnector();

$capacityandthresholds_query = "SELECT type, capacity FROM sf_capacity";

$capacities = $conn->Queries($capacityandthresholds_query);

$capacity = [];

foreach ($capacities as $_capacity) {
  $capacity[$_capacity['type']] = $_capacity['capacity'];
}

echo json_encode([
    'capacity' => $capacity,
]);
