<!-- old consumable status: calculate waring using predictive algoritm based on the past 7 days usages
    reason to stop using:
        1. not helpful for staffs, the staffs estimate when they should refill the kiosk based on remaining resources (receipt, notes)
        2. slow and unnecessary
-->

<?php
header("Access-Control-Allow-Origin: *");

include_once 'dbconnector.php';
$conn = new dbconnector();

/* get materials */
$materials_count_from_last_refill_query = "
SELECT
  k.id,
  k.ip,
  k.secondid,
  k.name,
  COUNT(DISTINCT IF(t.txtime > r.receiptrefilltime, t.id, null)) printedreceipts,
  SUM(IF(t.txtime > r.lakrefilltime, p.laknotespertransaction, 0)) insertedlaks,
  SUM(IF(t.txtime > r.thbrefilltime, p.thbnotespertransaction, 0)) insertedthbs,
  date_format(r.receiptrefilltime, '%d/%m %H:%i') receiptrefilltime,
  date_format(r.lakrefilltime, '%d/%m %H:%i') lakrefilltime,
  date_format(r.thbrefilltime, '%d/%m %H:%i') thbrefilltime
FROM sf_transaction t
JOIN sf_kiosk k ON t.userid = k.id AND k.name LIKE '%bound%'
LEFT JOIN (
  SELECT
    transaction_id,
    SUM(IF(pd.noteccy = 'LAK', pd.qty, 0)) laknotespertransaction,
    SUM(IF(pd.noteccy = 'THB', pd.qty, 0)) thbnotespertransaction
    FROM sf_payment p
    JOIN sf_paymentdetail pd ON p.id = pd.paymentid
    WHERE p.txtime > NOW() - INTERVAL 7 DAY
    GROUP BY p.transaction_id
  ) p ON t.id = p.transaction_id
JOIN (
  SELECT
    kioskid,
    MAX(IF (refilltype = 'RECEIPT', txtime, null)) receiptrefilltime,
    MAX(IF (refilltype = 'THB', txtime, null)) thbrefilltime,
    MAX(IF (refilltype = 'LAK', txtime, null)) lakrefilltime
    FROM sf_refill
    GROUP BY kioskid
  ) r ON t.userid = r.kioskid
WHERE t.txtime > NOW() - INTERVAL 30 DAY
GROUP BY t.userid
;
";
$materials_count_from_last_refill = $conn->Queries($materials_count_from_last_refill_query);


// each kiosk, find estimated empty time
for ($i = 0; $i < count($materials_count_from_last_refill); $i++) {

  $ests = $conn->Queries("
SELECT
  date_format(max(receipttime), '%d/%m %H:%i') receiptemptytime,
  date_format(max(laktime), '%d/%m %H:%i') lakoverflowtime,
  date_format(max(thbtime), '%d/%m %H:%i') thboverflowtime,
  max(receiptseconds) receiptemptytimeinseconds,
  max(lakseconds) lakoverflowtimeinseconds,
  max(thbseconds) thboverflowtimeinseconds
FROM (
  SELECT s2.*,
    IF (accreceipts < c.receiptcapacity - c.printedreceipts, est, null) receipttime,
    IF (acclaks < c.lakcapacity - c.insertedlaks, est, null) laktime,
    IF (accthbs < c.thbcapacity - c.insertedthbs, est, null) thbtime,

    IF (accreceipts < c.receiptcapacity - c.printedreceipts, totalseconds, null) receiptseconds,
    IF (acclaks < c.lakcapacity - c.insertedlaks, totalseconds, null) lakseconds,
    IF (accthbs < c.thbcapacity - c.insertedthbs, totalseconds, null) thbseconds
  FROM (
    SELECT
      userid,
      s.txtime,
      timediff,
      time_to_sec(timediff) totalseconds,
      addtime(now(), timediff) est,
      receipts,
      (@accreceipt := @accreceipt + receipts) accreceipts,
      laks,
      floor(@acclak := @acclak + laks) acclaks,
      thbs,
      floor(@accthb := @accthb + thbs) accthbs
    FROM (
      SELECT
        t.id,
        t.userid,
        t.txtime,
        TIMEDIFF(t.txtime, NOW() - INTERVAL 7 DAY ) timediff,
        COUNT(distinct t.id) receipts
      FROM sf_transaction t
      WHERE t.userid = :kioskid
        AND t.txtime > now() - interval 7 day
      GROUP BY t.id
      ORDER BY t.txtime
    ) s
    JOIN (
      SELECT
        t.id,
        t.txtime,
        SUM(IF(noteccy = 'LAK', qty, 0)) laks,
        SUM(IF(noteccy = 'THB', qty, 0)) thbs
      FROM sf_transaction t
      JOIN sf_payment p ON t.id = p.transaction_id
      LEFT JOIN sf_paymentdetail pd
        ON p.id = pd.paymentid
          AND t.paymentchannel = 'cash'
      WHERE t.userid = :kioskid_2
        AND t.txtime > now() - interval 7 day
      GROUP BY t.id
      ORDER BY t.txtime
    ) sc ON s.id = sc.id
    JOIN (SELECT @accreceipt := 0, @acclak := 0, @accthb := 0) c
  ) s2
  JOIN v_consumable c ON c.id = s2.userid
) s3
;", [
      "kioskid" => $materials_count_from_last_refill[$i]['id'],
      "kioskid_2" => $materials_count_from_last_refill[$i]['id']
  ]);

  $materials_count_from_last_refill[$i]['receiptemptytime'] = $ests[0]['receiptemptytime'];
  $materials_count_from_last_refill[$i]['lakoverflowtime'] = $ests[0]['lakoverflowtime'];
  $materials_count_from_last_refill[$i]['thboverflowtime'] = $ests[0]['thboverflowtime'];

  $materials_count_from_last_refill[$i]['receiptemptytimeinseconds'] = $ests[0]['receiptemptytimeinseconds'];
  $materials_count_from_last_refill[$i]['lakoverflowtimeinseconds'] = $ests[0]['lakoverflowtimeinseconds'];
  $materials_count_from_last_refill[$i]['thboverflowtimeinseconds'] = $ests[0]['thboverflowtimeinseconds'];
}

echo json_encode($materials_count_from_last_refill);
