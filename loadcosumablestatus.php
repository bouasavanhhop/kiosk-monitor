<?php
header("Access-Control-Allow-Origin: *");

include_once 'dbconnector.php';
$conn = new dbconnector();

/* get materials */
$materials_count_from_last_refill_query = "
SELECT k.id,
       k.ip,
       k.secondid,
       k.name,
#        printedpapers = printedreceipts + printedcoupons
       COUNT(DISTINCT IF(t.txtime > r.receiptrefilltime, t.id, null)) + SUM(IF(t.txtime > r.receiptrefilltime, p.printedpcouponspertransaction, 0)) printedreceipts,
       SUM(IF(t.txtime > r.lakrefilltime, p.laknotespertransaction, 0))            insertedlaks,
       SUM(IF(t.txtime > r.thbrefilltime, p.thbnotespertransaction, 0))            insertedthbs,
       date_format(r.receiptrefilltime, '%d/%m %H:%i')                             receiptrefilltime,
       date_format(r.lakrefilltime, '%d/%m %H:%i')                                 lakrefilltime,
       date_format(r.thbrefilltime, '%d/%m %H:%i')                                 thbrefilltime
FROM sf_transaction t
         JOIN sf_kiosk k ON t.userid = k.id AND k.name LIKE '%bound%'
         LEFT JOIN (
    SELECT id,
           transaction_id,
           SUM(IF(pd.noteccy = 'LAK', pd.qty, 0)) laknotespertransaction,
           SUM(IF(pd.noteccy = 'THB', pd.qty, 0)) thbnotespertransaction,
           SUM(IF(pd.type = 'newcoupon', 1, 0))   printedpcouponspertransaction
    FROM sf_payment p
             JOIN sf_paymentdetail pd ON p.id = pd.paymentid
    WHERE p.txtime > NOW() - INTERVAL 7 DAY
    GROUP BY p.transaction_id
) p ON t.id = p.transaction_id
         JOIN (
    SELECT kioskid,
           MAX(IF(refilltype = 'RECEIPT', txtime, null)) receiptrefilltime,
           MAX(IF(refilltype = 'THB', txtime, null))     thbrefilltime,
           MAX(IF(refilltype = 'LAK', txtime, null))     lakrefilltime
    FROM sf_refill
    GROUP BY kioskid
) r ON t.userid = r.kioskid
WHERE t.txtime > NOW() - INTERVAL 30 DAY
GROUP BY t.userid
";
$materials_count_from_last_refill = $conn->Queries($materials_count_from_last_refill_query);

echo json_encode($materials_count_from_last_refill);
