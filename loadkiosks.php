<?php
header("Access-Control-Allow-Origin: *");

include_once 'dbconnector.php';
$conn = new dbconnector();

/*get kiosks*/
$get_kiosks = [
    "sql" => "SELECT * FROM sf_kiosk 
                WHERE id IN (180, 181, 182, 183, 184, 185,186, 187, 188, 189)
                ORDER BY secondid"
];
$kiosks = $conn->Queries($get_kiosks["sql"]);

echo json_encode($kiosks);
